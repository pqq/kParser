# ------------------------------------------------------------------------------------
# filename: kParser.pl
# author:   Frode Klevstul (frode@klevstul.com) (http://klevstul.com)
# started:  27.10.2011
# ------------------------------------------------------------------------------------
#
# Revision - Newest version at the top - - - - - - - - - - - - - - - - - - - - - - - -
# version:  v01_20111027
#           - First version of this program was made.
# ------------------------------------------------------------------------------------

# -------------------
# use
# -------------------
use strict;


# -------------------
# declarations
# -------------------
my $debug 					= 1;																	# debug mode, on/off
my $iniFile					= "kParser.ini";														# the file containing all configuration values
my @inputData;
my @outputData;


# -------------------
# main program
# -------------------
loadInputFile();
parseData();
writeOutputFile();


# -------------------
# sub
# -------------------

# - - - - - - - - - - - - - - - - - - - - - - - - - - -
# load input file
# - - - - - - - - - - - - - - - - - - - - - - - - - - -
sub loadInputFile{
	my $file = getIniValue("DATA_FOLDER")."/".getIniValue("INPUT_FILE");
	my $line;

	open (FILE, "<$file") || kError("ERROR: Could not open '$file'", 1);
	while ($line = <FILE>){
		chomp($line);
		push(@inputData, $line);
	}
	close (FILE);
}


# - - - - - - - - - - - - - - - - - - - - - - - - - - -
# parse data
# - - - - - - - - - - - - - - - - - - - - - - - - - - -
sub parseData{
	my $targetString		= getIniValue("TARGET_STRING");
	my $delimiter			= getIniValue("DELIMITER");
	my $contains_header		= getIniValue("CONTAINS_HEADER");
	my $line;
	my @lineArray;
	my $outputLine;
	my $i;
	my $lineNo				= 0;

	foreach $line (@inputData){
		if ($contains_header eq "true" && $lineNo == 0){
			kDebug("skipping header line");
			$lineNo++;
			next;
		}

		@lineArray = split($delimiter, $line);

		$outputLine = $targetString;

		kDebug($outputLine);
		for $i (1 .. $#lineArray+1){
			kDebug("replacing %$i with $lineArray[$i-1]");
			$outputLine =~ s/%$i/$lineArray[$i-1]/sgi;
		}

		push(@outputData, $outputLine);
		$lineNo++;
	}
}


# - - - - - - - - - - - - - - - - - - - - - - - - - - -
# write output file
# - - - - - - - - - - - - - - - - - - - - - - - - - - -
sub writeOutputFile{
	my $file = getIniValue("DATA_FOLDER")."/".getIniValue("OUTPUT_FILE");

	open (FILE, ">$file") || kError("ERROR: Could not open '$file'", 1);
	foreach (@outputData){
		print FILE $_ ."\n";
	}
	close (FILE);
}


# - - - - - - - - - - - - - - - - - - - - - - - - - - -
# load initial config values
# - - - - - - - - - - - - - - - - - - - - - - - - - - -
sub getIniValue{
	my $parameter	= $_[0];
	my $list		= $_[1];
	my $line;
	my $value;

	$parameter =~ tr/a-z/A-Z/;																		# convert the parameter to uppercase

    open(FILE, "<$iniFile") || kError("failed to open '$iniFile'", 1);
	WHILE: while ($line = <FILE>){
		if ($line =~ m/^#/){
			next WHILE;
		}

		if ($list){
			if ($line =~ m/^(\w+)(\t)+(.*)$/){
				print $1 . " : " . $3 . "\n";
			}
		} else {
			if ($line =~ m/^$parameter(\t)+(.*)$/){
				$value = $2;
				last WHILE;
			}
		}
	}
	close (FILE);

	if ($value eq "" && !$list){
		kError("Failed to find the values for module '$parameter' in '$iniFile'",1);
	}

	kDebug("'$parameter' values read from .ini file");

	return $value;
}


# - - - - - - - - - - - - - - - - - - - - - - - - - - -
# Prints out debug message
# - - - - - - - - - - - - - - - - - - - - - - - - - - -
sub kDebug{
	my $text = $_[0];

	if ($debug){
		print("DEBUG: ".$text."\n");
	}
}



# - - - - - - - - - - - - - - - - - - - - - - - - - - -
# Prints out error message
# - - - - - - - - - - - - - - - - - - - - - - - - - - -
sub kError{
	my $text = $_[0];
	my $exit = $_[1];

	print("ERROR: ".$text);

	if ($exit){
		exit;
	}
}
